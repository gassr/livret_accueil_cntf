.. index::
   ! meta-infos


.. _doc_meta_infos:

=====================
Meta infos
=====================

.. seealso::

   - https://cnt-f.gitlab.io/meta/




Gitlab project
================

.. seealso::

   - https://gitlab.com/cnt-f/livret_accueil


Pipelines
----------

- https://gitlab.com/cnt-f/livret_accueil/-/pipelines

Issues
--------

.. seealso::

   - https://gitlab.com/cnt-f/livret_accueil/-/boards


pyproject.toml
=================

.. literalinclude:: ../pyproject.toml
   :linenos:

conf.py
========

.. literalinclude:: ../conf.py
   :linenos:


gitlab-ci.yaml
===============

.. literalinclude:: ../.gitlab-ci.yml
   :linenos:


.pre-commit-config.yaml
========================

.. literalinclude:: ../.pre-commit-config.yaml
   :linenos:


Makefile
==========

.. literalinclude:: ../Makefile
   :linenos:

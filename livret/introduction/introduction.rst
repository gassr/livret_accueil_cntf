
.. index::
   pair: Livret d'accueil; Introduction

.. _livret_accueil_intro:

======================
Introduction
======================


L’adhésion à la CNT se fait auprès du :term:`syndicat` de branche (éducation, services,
bâtiment, transport, métallurgie, etc.) correspondant aux études ou à l’activité
du travailleur avec ou sans emploi, étudiant, chômeur, retraité.

Si le :term:`syndicat` de branche n’existe pas, l’adhésion se fait auprès
du :term:`syndicat` interprofessionnel (ce dernier regroupe les
travailleuses/travailleurs de plusieurs branches).

Ces :term:`syndicats` dits *interpro* ou *interco* ont comme sigles
:term:`ETPICS`, :term:`STICS` ou :term:`STIS`.

Tous les :term:`syndicats` adhérents de la CNT ont des statuts en accord avec
:ref:`l’article premier des statuts de la Confédération <titre_1_statuts_cnt>`.

Bienvenue donc au sein de la CNT.

Tu as choisi comme premier engagement syndical, ou après des expériences
négatives dans d’autres organisations professionnelles, de rejoindre la CNT
et nous nous réjouissons de ce choix.
Ce court livret a pour but de faciliter ton insertion au sein de notre syndicat,
il est complémentaire des statuts confédéraux.

L’engagement syndical se traduit par un premier acte militant: s’acquitter
de sa cotisation.
Ensuite il faut participer, dans la mesure de ses disponibilités, à la vie
du :term:`syndicat`.

Notre syndicalisme ne se limite nullement à l’activité dans l’entreprise
mais s’exerce sur le plan géographique,au sein des Unions locales,
départementales ou régionales, lieu d’actions unitaires entre :term:`syndicats` de
branche ou non d’horizons différents.

Ce qui caractérise en premier lieu le fonctionnement de la CNT, c’est
son projet de société :term:`autogestionnaire`, ce qui se traduit **ici et maintenant**
dans le syndicat qui est autogéré,comme dans les luttes où nous militons
pour leur auto-organisation.

À la CNT ce sont les :term:`syndicats` qui  administrent la Confédération,
sans recours à des permanents ou à des salariés.

Pour  en  savoir plus, s’informer de l’intégralité des :ref:`statuts <statuts:cnt_statuts>`
de  la Confédération, de l’union régionale, de la Fédération et de son syndicat
d’appartenance est indispensable.

Mais ce qui forge la CNT et ses  adhérents, ce sont surtout les actions
pour l’amélioration  des conditions  des travailleurs, avec ou  sans
emploi, la poursuite du combat pour un **monde meilleur** et la
**solidarité entre ses membres**.

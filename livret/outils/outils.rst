

.. index::
   pair: Livret d'accueil; Outil
   pair: Listes de discussion; netiquette
   pair: Listes de discussion; outil
   pair: intranet ; outil
   pair: Livret d'accueil; Radio libertaire

.. _livret_accueil_outils:

============================================================
Des outils pour la lutte
============================================================




Le combat syndicaliste
=======================

.. seealso::

   - http://www.cnt-f.org/-le-combat-syndicaliste-.html


La Confédération réalise chaque mois un journal **Le Combat syndicaliste**
auquel on peut s’abonner auprès d’un militant de la CNT.


Sites Web
===========

.. seealso::

   - http://www.cnt-f.org/annuaire-des-syndicats-et-unions-locales-de-la-cnt.html

- confédération : http://cnt-f.org
- région parisienne : http://cnt-f.org/urp/
- Secrétariat international : http://cnt-f.org/international
- Secteur vidéo : http://cnt-f.org/video

Sites Web de Rhône-Alpes
-------------------------

- de la CNT38 : http://www.cnt-f.org/ul38/



Listes de discussion
=====================

.. seealso::

   - https://fr.wikipedia.org/wiki/Liste_de_diffusion


Une liste interne "syndicats" qui permet aux syndicats CNT d'échanger des
informations.

Une liste interne "stics38" qui permet aux militanTEs du STICS38 d'échanger
des informations.

Une liste interne "ud38" qui permet aux militanTEs de la CNT38 d'échanger des
informations.

La participation aux listes requiert d'appliquer la `netiquette`_ et donc de
ne pas lancer de trolls_.


.. _netiquette:

La netiquette
================

.. seealso::

   - https://fr.wikipedia.org/wiki/N%C3%A9tiquette
   - https://fr.wikipedia.org/wiki/Troll_(Internet)
   - https://fr.wikipedia.org/wiki/%C3%89thique
   - https://fr.wikipedia.org/wiki/Sociologie_des_communaut%C3%A9s_virtuelles


Définition
------------

S’il ne fallait retenir qu’une règle : **ce que vous ne feriez pas lors d’une
conversation réelle face à votre correspondant, ne prenez pas l’Internet
comme bouclier pour le faire**.

À cette notion de courtoisie et de respect de l’autre viennent ensuite se
greffer des règles supplémentaires relatives aux spécificités de plusieurs
médias.

Ces règles n’ont cependant pas été actualisées pour couvrir les médias plus
récents (forums, wikis, blogs, vidéo-conférences, etc.), les standards plus
récents (Unicode, XML, etc.) ni les technologies plus récentes (haut débit,
VoIP, etc.)


.. _troll:
.. _trolls:

Ne pas troller
================

.. seealso::

   - https://fr.wikipedia.org/wiki/Troll_(Internet)


Définition
-------------

En argot Internet, **un troll caractérise ce qui vise à générer des polémiques**.

Il peut s'agir d'un message (par exemple sur un forum), d'un débat conflictuel
dans son ensemble ou de la personne qui en est à l'origine.

Ainsi, *troller*, c'est créer artificiellement une controverse qui focalise
l'attention aux dépens des échanges et de l'équilibre habituel de la communauté.

Désigner un *troll* est un jugement subjectif, la désignation d'un élément
sciemment perturbateur pour le discréditer et l'éviter.

**L’argumentation caricaturale et récurrente** sont les **empreintes typiques**
d'un troll. Ils sont la preuve d'une mécommunication, et d'une impossibilité
d'échange dans la compréhension mutuelle, mais le *trollage* présume en
plus des **provocations intentionnelles et le but de nuire**.

À l'origine, le terme renvoi à une plaisanterie (*troll positif*) où le
trolleur tire satisfaction d'avoir réussi à berner ses victimes, à leur avoir
fait perdre du temps. Son champ s'élargit à partir des années 2010 et il
peut dorénavant aussi s'appliquer à l'envoi de messages provocateurs
et offensants, exacerbés par l'anonymat et la tribune que procure internet
(*troll négatif*). Cette seconde définition s'apparente au flaming.


Intranet
=========

Un intranet permet d'accéder à des documents internes.

Radios
=======

Radios Libertaire
-------------------

.. seealso::

   - http://media.radio-libertaire.org:8080/radiolib.mp3
   - https://www.radio-libertaire.net/
   - https://www.federation-anarchiste.org/rl/prog/



Les syndicats de la CNT animent une émission sur Radio libertaire les mardis
de 20h30 à 22h30

- 1er mardi : CNT-RP
- 2e mardi : CNT éducation
- 4e mardi : Sévices publics (Énergie)

La CNT 94 anime le 5e dimanche l’émission Micro-Ondes 94 de 15h30 à 17 heures

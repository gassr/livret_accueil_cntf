

.. index::
   pair: Livret d'accueil; Glossaire

.. _livret_accueil_glossaire:

============================================================
Glossaire des abbréviations les plus fréquemment utilisées
============================================================


- :ref:`congres:glossaire_cnt`
- :ref:`juridique:glossaire_juridique`




.. index::
   pair: Livret d'accueil; Structures

.. _livret_accueil_structures:

============================================================
Les différentes structures organisationnelles de la CNT
============================================================

.. contents::
  :depth: 3

Le syndicat
=============

Le syndicat: C’est le groupement de base décisionnel de la CNT, qui
regroupe les adhérents d’un même secteur d’activité.

Exemples:

- :term:`syndicats` des PTT de Paris, du Rhône de Santé-Social de Gironde,
  etc.

Dans une branche professionnelle donnée, le :term:`syndicat` regroupe les salariés
et ce, quels que soient le statut ou l’échelon de chacun.

Sur un site considéré, la section syndicale (de l’école, de l’usine, du bureau
etc.) syndique également les travailleurs d’entreprises sous-traitante,
par exemple des agents du nettoyage, des salariés en formation.

Le :term:`syndicat` élit un bureau comprenant au moins deux personnes, un secrétaire,
un trésorier.
Le :term:`syndicat` est obligatoirement déclaré en mairie, parfois préfecture.

**C’est le syndicat qui peut alors déclarer les sections d’entreprises**.

*Une fois par an, le syndicat peut tenir un congrès*, c’est-à-dire une
assemblée  générale pour faire le bilan de son action, choisir des orientations
pour l’année à venir, élire un nouveau bureau du syndicat.


L’union locale ou UL
=======================

L’union locale regroupe des :term:`syndicats` sur un plan territorial ou
géographique.

C’est donc un lieu interprofessionnel où des :term:`syndicats` de branches
professionnelles différentes se retrouvent, dépassant le combat catégoriel
pour lutter ensemble sur des revendications communes de type économiques
(retraites, temps de travail, etc.) ou plus politiques:

- combat contre le racisme,
- défense de locataires ou mal-logés, de sans-papiers,
- antifascisme,
- solidarité,
- etc.

En principe les UL se constituent sur une ville ou un secteur géographique.

C’est au niveau de l’UL que se décident les orientations locales pour
développer la CNT sur la ville.

L’union départementale ou UD
==============================


Il s’agit sur le plan département d’un regroupement de :term:`syndicats`
départementaux de branches avec éventuellement des UL.
C’est à ce niveau que se définissent les orientations pour assurer le
développement de la CNT sur la zone géographique.

L’union régionale ou UR
========================


C’est le regroupement sur un plan géographique des :term:`syndicats` d’une
même zone qui peut regrouper plusieurs départements.

C’est le lieu de coordination de l’action de la CNT sur le plan de la région.


La fédération
==============

Les :term:`syndicats` d’une même branche professionnelle se structurent sur le
plan national en fédération.

Exemples: fédération des :term:`syndicats` CNT éducation, PTT, etc.

C’est au niveau fédéral que sont adoptées les revendications spécifiques à
la branche, les formes de luttes proposées, les outils de propagande
propres à la branche (journal, tract, bulletin, etc.)

La Confédération
==================

La Confédération rassemble l’ensemble des :term:`syndicats` et fédérations
adhérents de la CNT.

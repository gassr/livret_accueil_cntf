

.. index::
   pair: Livret d'accueil; Moyens

.. _livret_accueil_moyens:

===================================
Différents moyens de militer
===================================


Il y a plusieurs façons de participer à l’action militante:

- lire (s’abonner) à la presse de la Confédération,
- consulter les sites internet de la CNT pour s’informer,
- participer aux réunions du syndicat,
- partager des expériences,
- signaler des luttes locales,
- diffuser des tracts,
- tenir à jour le panneau d’affichage dans son entreprise,
- participer à un collage d’affiches dans son quartier.


L’ investissement militant ce la peut être aussi venir à une manifestation,
animer la vie de sa section syndicale, préparer un texte pour susciter un échange
dans le syndicat ou pour être publié dans la presse syndicale ou sur un site
CNT (compte-rendu d’une lutte, présentation d’un livre ou d’un film, etc.).

Progressivement prendre un mandat dans le syndicat, participer à un Congrès
sont d’autres formes de l’engagement.

L’activité collective permet d’acquérir une expérience, des connaissances,
de partager et de développer des liens de **solidarité**.

Ainsi, il est possible de participer à des groupes de travail (transverse aux
syndicats) qui étudient et proposent des textes de réflexion et des tracts à
la CNT mais aussi de participer aux sessions de formation régulièrement
organisées.

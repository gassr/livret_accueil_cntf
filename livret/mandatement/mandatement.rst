

.. index::
   pair: Livret d'accueil; Mandatement

.. _livret_mandatement:

========================================================
Mandatements, autogestion et démocratie directe
========================================================


À la CNT il n’y a pas de représentants qui sont permanents, détachés du travail
et  payés pour faire du syndicalisme.
À chaque niveau des structures de la CNT (syndicat, union locale, région,
fédération,  Confédération,  secrétariat international,  etc.), des  personnes
sont  donc mandatées pour gérer au nom du syndicat une tâche bien déterminée,
et durant une période précise: secrétariat, trésorerie, accueil des  contacts,
gestion  juridique,  service  d’ordre pour  les  manifestations, etc.

Ces personnes, comme tous les mandatés, sont révocables à tout moment par la
structure qui les a désignées. Les modalités de la révocation sont précisées dans
les statuts.

Notre Confédération préconise la mise en place de la rotation des mandats et des
tâches.

Cela veut dire réaliser une formation pour que chaque adhérent puisse
à son tour prendre en charge tel ou tel mandat, telle ou telle tâche, afin de
partager les responsabilités, et éviter la spécialisation des uns ou des autres.
Il s’agit de permettre à chacun de s’investir en portant  des  responsabilités
en  fonction  de  ses capacités et de ses disponibilités



.. index::
  pair: Livret d'accueil; Fonctionnement

.. _livret_accueil_fonctionnement:

========================================================
Fonctionnement de chaque structure de la CNT
========================================================


Chaque structure choisit le mode de fonctionnement le plus adapté à sa réalité.
Mais en principe un :term:`syndicat`, une Union locale, départementale, régionale,
une fédération comme la Confédération ont des règles proches.

Chaque structure désigne un bureau, ou une commission exécutive, comprenant à
minima un secrétaire, un trésorier.
Selon la structure le bureau est plus ou moins étoffé avec par exemple plusieurs
mandatéEs.

Exemples pour la Fédération des activités du courrier et de la téléphonie un
bureau qui comprend en plus des mandatés au site internet, à la propagande,
aux relations internationales.

Le Bureau confédéral comporte un secteur propagande, un Secrétariat international,
etc.

**Chaque structure tient généralement un congrès** mais à des rythmes différents:

- **congrès annuel** pour un :term:`syndicat`, une région, une union locale ;
- **congrès bisannuel** fédération PTT, Confédération.

Lors d’un **congrès** chaque :term:`syndicat` est représenté par un ou deux mandatés.

Le **congrès** définit les orientations de la structure pour l’année, ou les
années à venir, ainsi que les modalités de fonctionnement.
Il est préparé dans chaque :term:`syndicat` à partir de textes proposés par des
:term:`syndicats`, que l’on appelle *motions* et qui sont débattues, amendées, adoptées
ou rejetées en congrès.

C’est lors d’un **congrès** que sont élus les mandatés pour le bureau de la
structure, c’est là qu’ils présentent un bilan de leurs mandats, comme
les :term:`syndicats` présentent un rapport d’activités depuis le congrès précédent.

La Confédération est gérée par un **Bureau confédéral (BC)**: celui-ci n’a aucun
pouvoir de décision dans la Confédération.

Entre deux Congrès pour certaines structures il existe des réunions spécifiques
Certaines fédérations, comme celle des PTT, organisent une commission administrative
fédérale (Caf), tous les six mois, qui réunit des représentants des :term:`syndicats`,
le bureau fédéral, et qui traite de questions techniques, réalise du matériel
pour l’intervention (tracts, brochures, affiches, autocollants)

Entre deux **congrès**, la Confédération se réunit en **Comité Confédéral National (CCN)**,
tous les six mois, et il regroupe les membres du BC, des fédérations, du
comité de rédaction du journal de la CNT, et des délégués des unions régionales
qui sont les seuls habilités à voter et donc à prendre des décisions.

Le :term:`CCN` fait le point sur l’application des décisions prises lors du
:ref:`Congrès précédent <congres_CNT>`, détermine le choix de campagnes confédérales.
Ces réunions semestrielles sont préparées dans les :term:`syndicats` et surtout dans
les unions régionales où sont choisis les mandatés.

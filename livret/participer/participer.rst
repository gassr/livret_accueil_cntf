

.. index::
   pair: Livret d'accueil; Participer

.. _adherer_participer:

========================================================
Adhérer, c’est aussi participer à la vie du syndicat
========================================================

.. seealso:

   - :ref:`glossaire <glossaire_cnt>`



Introduction
==============


Dans  notre  Confédération,  les  décisions  sont prises en **assemblée générale**
de :term:`syndicat` et les mandatés s’en tiennent aux décisions prises par les
syndicats.

Rien ne vient d’en *haut* car il n’y a pas de *haut* à la CNT.

Participer dans la mesure de ses possibilités aux réunions permet de faire
entendre son point de vue et d’écouter celui des autres

.. _fct_reunion_syndicale:

Une réunion syndicale ça fonctionne comment ?
================================================

Chacun doit pouvoir exposer sa position, ses idées, c’est pourquoi un tour
de paroles est organisé et, à l’issue des échanges, une décision est prise
collectivement.

On recherche le plus souvent un consensus mais en cas de désaccords on procède
à un vote.

Durant la réunion, une personne prend des notes pour  effectuer une  synthèse
et recenser les décisions prises ce  qui  permettra de s’y reporter en particulier
concernant le **qui fait quoi**

Pour effectuer ces tâches (tour de paroles, compte-rendu), il  convient à
chaque  réunion  de  faire  tourner  ces mandats afin que chacun participe
à l’autogestion du syndicat.

Mais à la CNT, c’est la liberté qui prévaut et donc les **obligations sont
celles que chaque adhérent se donne**.

La prise de notes ainsi effectuée est envoyée à chaque membre du syndicat, de
cette façon, même si on n’a pas pu assister à la réunion, on est informé.
Il faut, lors des envois, ne pas oublier les personnes qui n’ont pas Internet
et procéder à l’envoi d’un courrier postal.

Pour décider, donner un point de  vue, l’information doit circuler à l’intérieur
du  syndicat.

Ainsi les circulaires fédérales, régionales, confédérales ou internationales
doivent être mises à disposition, par courrier, courriel et aussi dans le local
quand le :term:`syndicat` en  possède un.

De  même, une explication des sigles utilisés est un impératif pour la clarté
des débats. À ce propos, voir le :ref:`glossaire <glossaire_cnt>`.

En  conclusion, la bonne marche d’un :term:`syndicat` CNT se vérifie à la bonne
tenue de sa trésorerie et au cadre autogestionnaire de celui-ci reposant sur
trois éléments indissociables:

1/ **l’ordre du jour** de l’AG élaboré par les adhérents eux-mêmes et connu de
   tous **environ huit jours à l’avance**

2/ l’:term:`AG` elle-même qui se déroule conformément aux statuts du syndicat où,
   outre les décisions prises, on établit **le qui fait quoi?**

3/ le CR de l’:term:`AG` qui parvient à tous les adhérents environ huit jours après
   et permet à tous les adhérents d’être à égalité d’information.



.. index::
   pair: Livret d'accueil; AGs

.. _livret_accueil_ags:

==========================================================================
L'assemblée générale du syndicat: lieu de décision et de vie du syndicat
==========================================================================


En général, le :term:`syndicat` se réunit une fois par mois pour décider de son
activité, des actions à organiser, des moyens à mettre en œuvre pour se
développer dans un lieu de travail ou un quartier.
C’est là que l’on fait le point sur la situation dans son entreprise, dans le
quartier, que l’on échange.

C’est le lieu de préparation des actions syndicales (réalisation de
tracts, de bulletins, d’affiches, réunion publique, projection d’un film,
concert, etc.).

L’:term:`AG` peut aussi être un lieu de réflexion et d’échanges:

- analyse de la période,
- montée de l’extrême droite,
- loi travail,
- commentaires sur le journal de la Confédération,
- rédaction d’un article pour ce journal...
- etc...

L’:term:`assemblée générale` est préparée par l’envoi à chaque adhérent
d’un ordre du jour proposé, que **chacun peut compléter**.

À la fin de la réunion la date de la prochaine :term:`AG` est fixée.

Le compte-rendu de la réunion est envoyé à tous les adhérents.

Régulièrement, au cours d’un congrès du :term:`syndicat` ou d’une :term:`AG` annuelle,
est adopté le choix du secrétariat ou du trésorier tels que définis
dans les statuts du :term:`syndicat`.

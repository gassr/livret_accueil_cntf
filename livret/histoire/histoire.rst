

.. index::
  pair: Livret d'accueil; Histoire

.. _livret_accueil_histoire:

========================================================
Et la CNT, d'où vient-elle ?
========================================================

.. seealso::

   - http://www.cnt-f.org/presentation.html

Un peu d’histoire...

La CNT française a été créée en 1946 et se situe dans la continuité des principes
et du fonctionnement de la CGT à ses origines, avant qu’elle ne devienne un
syndicat institutionnel, de la CGT-SR qui entre les deux guerres s’est
constituée pour refuser la mise sous tutelle politique de la CGT.

Mais la création de la CNT est aussi marquée par la grande sœur (CNT espagnole)
qui outre des combats syndicaux marquants depuis le début du XXe siècle s’est
illustrée dans le camp républicain espagnol de 1936 à 1939 pendant la guerre
civile.
Les arnarcho-syndicalistes ont alors mis en application, en grande partie, ce
qu’ils avaient défini dans le communisme libertaire.
Ils ont allié la propriété collective non étatique des moyens de production et
de distribution à la démocratie directe et à la liberté individuelle.
Pour exemple : l’autogestion industrielle en Catalogne et les communautés
agraires en Aragon et Andalousie.

Ce n’est qu’à la fin des années 1980 et lors du mouvement de grèves de décembre
1995 contre la réforme Juppé des retraites que la CNT a commencé à se redévelopper
dans différents secteurs professionnels.
Aujourd’hui notre Confédération demeure une organisation modeste qu’il s’agit
d’implanter durablement dans les entreprises et les quartiers.
Tu l’as lu dans :ref:`l’article premier des statuts <titre_1_statuts_cnt>`.

La CNT est **une organisation de classe** qui accueille tous les travailleurs à
l’exception de ceux des forces répressives. Donc tous les travailleurs ont leur
place à la CNT s’ils combattent pour l’amélioration de la condition des
travailleurs et **s’ils sont pour l’égalité entre tous les êtres humains**.

Oui, nous revendiquons que les travailleurs peuvent collectivement et sans
leaders changer cette société pour savoir comment, pourquoi et pour qui
produire biens et services... sans profits !

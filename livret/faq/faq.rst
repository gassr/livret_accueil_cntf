.. index::
   ! FAQ

.. _faq_cnt:

========================================================
FAQ
========================================================


Quelques réponses à des questions que se posent parfois celles et ceux
qui souhaitent nous rejoindre ou qui viennent de le faire

.. contents::
  :depth: 3

À quoi on joue ? Et qui s'y colle ?
=========================================

Tu es sur le point d'adhérer ou tu viens d'adhérer à la CNT.

Notre organisation syndicale possède un mode de fonctionnement que
chaque adhérent-e doit pouvoir connaître puisque notre volonté est
d'associer chacun-e aux prises de décision et de répartir les tâches
et les responsabilités.

L'originalité de notre organisation syndicale tient à la mise en
pratique de ce constat::

    **Puisque personne ne travaille à ta place, que personne ne décide à
    ta place**.

Chacun-e dans l'organisation syndicale a les mêmes droits et les mêmes devoirs.

La remise en cause de cette pratique ainsi que la création par d'autres
organisations syndicales d'une bureaucratie, de détaché-e-s à plein temps
et de permanent-e-s inamovibles a conduit les salarié-e-s à repousser
en masse le choix de se syndiquer (en France, 6 à 7% seulement des
salarié-e-s sont syndiqué-e-s).

Ainsi, la mise en place de ce corps de permanents syndicaux (soi-disant
pour accroître l'efficacité de l'organisation syndicale) a abouti aux
résultats contraires :

- éloigner l'adhérent-e du syndicat en lui retirant tous les pouvoirs
  de décision et de participation à l'action syndicale,
- faire du/de la permanent-e syndical-e un corps étranger au monde du
  travail et à ses aspirations, monde auquel il/elle ne comprend plus
  rien puisqu'il/elle n'en partage plus le quotidien.

Tu trouveras ci-dessous l'article 1 de nos statuts confédéraux.

La lecture de cet article est indispensable pour que chacun-e soit à même
de juger des choix fondateurs de notre organisation syndicale et de décider
d'y adhérer ou non.

Article 1 des statuts confédéraux de la CNT
============================================

La Confédération Nationale du Travail a pour but:

- De  grouper,  sur  le  terrain  spécifiquement  économique, sans  autre
  forme  de  discrimination, pour la défense de leurs intérêts matériels
  et moraux, tous les travailleurs/ses à l'exception des employeurs/ses
  et des forces répressives de l'État considéré-e-s comme des ennemi-e-s des travailleurs/ses.
- De poursuivre, par la lutte de classes et l'action directe, la libération
  des travailleurs/ses qui ne sera réalisée que par la transformation
  totale de la société actuelle.

Elle  précise  que  cette  transformation  ne  s'accomplira  que  par
la  suppression  du  salariat  et  du  patronat,  par  la syndicalisation
des moyens de production, de répartition, d'échange et de consommation,
et le remplacement de l'État par un organisme issu du syndicalisme lui-même
et géré par l'ensemble de la société.

La Confédération Nationale du Travail reposant sur le producteur/trice,
garantit à celui/celle-ci la direction de l'organisation des travailleurs/ses.

Elle  est  indépendante  de  tout  type  d'organisation  politique,
religieuse  ou  autre;  ce  qui  implique  que  tout-e adhérent-e ne
peut agir à la CNT au nom d'autres organisations.

La CNT, préconisant l'internationalisme comme moyen d'émancipation,
collabore à l'étude des questions sociales et économiques et œuvre à la
libération des travailleurs/ses, à l'échelle internationale.

La CNT développe la culture, l'instruction et la conscience de classe
des travailleurs/ses et entretient la solidarité parmi eux/elles.

En termes de modalités de prise de décision, les personnes ne nous
connaissant pas ou peu pensent parfois que nous n'avons jamais recours
au vote.
Cela est inexact. Dans les réunions intersyndicales, dans les assemblées
générales de salarié-e-s, dans toutes les instances de notre organisation
(congrès confédéraux, fédéraux, réunions d'union régionale, réunions de
syndicat ou réunions de section...), nous avons recours au vote dès que
ce procédé est nécessaire pour trancher.

Il est vrai que nous ne mettons pas la charrue avant les bœufs et que la
possibilité du vote ne doit pas faire oublier d'épuiser le débat qui
lui seul permet d'arriver à un accord, si ce n'est à un consensus
toujours souhaitable.


La CNT, un syndicat  **représentatif** ou non **représentatif** ?
=====================================================================

La position de la CNT face aux IRP (Instances Représentatives du Personnel)
est fondée à la fois sur des principes de fonctionnement et une approche
pratique due aux évolutions de la législation.

Le principe fondamental pour la CNT dans son analyse des IRP est son refus
de la délégation de pouvoir et de la démocratie représentative,
puisqu'elle défend l'autogestion (dénommée aussi **démocratie directe**)
dans son organisation interne et dans ses pratiques de lutte: mandatement,
révocabilité, partage et rotation des tâches, refus des spécialisations
ainsi que des permanent-e-s ou salarié-e-s de syndicats.

La CNT refuse également la cogestion, au sens du mythe d'un possible intérêt
commun entre le Capital et le Travail, entre patrons et salarié-e-s.

L'approche pratique est celle de pouvoir exister syndicalement dans les
lieux de travail, voire de protéger ses militant-e-s contre la répression
patronale.
De ce fait, la CNT a longtemps privilégié le mandat de DS (délégué-e syndical-e),
tout en critiquant et se méfiant ouvertement des IRP électives (DP et délégué-e au CE).

La CNT a ensuite accepté, pour des raisons de protection et d'implantation
syndicale, le recours aux DP (délégué-e du personnel).
Elle a refusé en revanche le plus longtemps possible la participation
au CE (comité d'entreprise), sauf exceptions réellement justifiées.

Mais la loi du 20 août 2008 de refonte du droit syndical l'a forcée à
aménager son positionnement.
En effet, cette loi conditionne l'existence des syndicats à la participation
aux élections professionnelles, et notamment au CE.

C'est l'objet du congrès de Lille de 2008 qui, tout en maintenant la
critique des IRP électives et en rappelant les principes fondateurs de
la CNT, prend acte de cette loi et accepte la présentation aux élections
de DP et CE.
Avec comme réserve de faire des bilans réguliers de cette participation
et de chercher les moyens d'éviter les dérives de délégation de pouvoir
et de cogestion que cela pourrait entraîner.

Dans le secteur public, la représentativité juridique s'acquiert par
les résultats obtenus aux élections professionnelles. 
En l'état actuel de nos prises de décisions confédérales, seules les
fédérations CNT estimant que leur existence est en danger et/ou que leurs
militant-e-s sont menacé-e-s peuvent présenter des listes à ces élections.

Néanmoins, même s'ils n'ont pas d'élu-e-s au sein des commissions paritaires,
tous les syndicats CNT du secteur public possèdent des droits a minima
dans les établissements où ils sont implantés : droit d'afficher, de
distribuer des tracts syndicaux, d'utiliser le local syndical commun,
d'organiser des réunions d'informations syndicales à l'intérieur de
l'établissement (en dehors des heures de travail des participant-e-s et
même pendant lorsque le rapport de force nous est localement favorable !).

Dans le secteur privé, la loi **portant rénovation de la démocratie sociale
et réforme du temps de travail**, effective depuis le 20 août 2008, a
refondé la notion de représentativité dans les entreprises.

Sans rentrer dans les débats politiques et idéologiques sur les motivations
générales de cette loi, largement critiquables (cf. la motion votée au
congrès confédéral CNT de septembre 2008), il est un élément qui pour la
CNT constitue une certaine forme d'avancée.

Il s'agit du ou de la **RSS (représentant-e de la section syndicale)**.

En effet, avant la loi de 2008, seules cinq organisations syndicales
bénéficiaient automatiquement de la représentativité syndicale dans
toutes les entreprises et quel que soit leur nombre d'adhérent-e-s.
Elles pouvaient donc désigner des DS sans risquer de voir leurs désignations
contestées.

Par contre, pour qu'un syndicat comme la CNT puisse exister légalement
dans une entreprise, il fallait créer une section plus ou moins clandestine,
s'assurer de remplir les critères de représentativité (activité, ancienneté
et nombre d'adhérent-e-s notamment) et désigner un-e DS.

Mais cette désignation était très souvent attaquée par l'employeur devant
un tribunal d'instance pour non-représentativité du syndicat.
Depuis le 20 août 2008, tous les syndicats, y-compris les syndicats non
représentatifs nationalement ou dans la branche d'industrie concernée,
peuvent constituer légalement une section syndicale.

Seules quatre conditions sont exigées:

- l'indépendance ;
- le respect des valeurs républicaines * ;
- être affilié à un syndicat légalement constitué depuis au moins deux
  ans et comprenant l'entreprise dans son champ professionnel et géographique ;
- avoir plusieurs adhérent-e-s dans l'entreprise.

Dans la foulée,  un-e représentant-e  de la section syndicale
(RSS) est désigné-e.

D'autre part, lors des élections professionnelles (dans les entreprises
comptant au moins 11 salarié-e-s), la CNT a la possibilité de présenter
une liste. Si cette liste obtient au moins 10% des voix, la CNT est
déclarée représentative dans l'entreprise et elle peut désigner un-e DS
(délégué-e syndical-e).

Concernant le **respect des valeurs républicaines**, plusieurs employeurs
ont tenté de contester la déclaration d'une section CNT au sein de leur
entreprise en arguant du fait, auprès d'un tribunal d'instance, que la
CNT ne satisfaisait pas à ce critère.

Mais, à chaque fois, ces employeurs ont été déboutés par la Justice et
ces jugements font maintenant jurisprudence.


La CNT, un syndicat politique ? La CNT, un syndicat apolitique ?
====================================================================

Pendant des décennies, l'impérialisme des partis politiques au sein du
mouvement social a fait de gros dégâts.
La  CNT n'a jamais partagé cette vision des choses car, pour nous, le
syndicalisme n'est pas un mode mineur.
Il semblerait qu'aujourd'hui, de plus en plus nombreux, sont ceux et
celles qui pensent, avec raison, pouvoir se passer de l'avis de toutes
les **avant-gardes** plus ou moins éclairées.

Reprenant à son compte la Charte d'Amiens, à laquelle d'autres organisations
syndicales actuelles ont aussi adhéré en leur temps, la CNT **déclare
que le syndicat, aujourd'hui groupement de résistance, sera dans l'avenir,
le groupement de production et de répartition, base de la réorganisation sociale**.

Cela ne signifie donc pas que la CNT n'a pas d'action politique.
Elle dénie le droit aux partis politiques de s'arroger le pouvoir de
transformer la société.
Tout membre de la CNT a le droit d'adhérer  (ou non) à un parti politique,
mais les partis politiques n'ont aucun droit à l'intérieur de
l'organisation syndicale.
Un article des statuts d'un syndicat CNT de la région parisienne résume
bien cette distinction: **Le syndicat s'autorise dans ses réunions toute
analyse de la situation  politique, économique, sociale, culturelle
française et internationale.
Mais s'interdit toute adhésion à des organisations politiques, philosophiques ou religieuses**.

Notre organisation se revendique de l'anarcho-syndicalisme et du
syndicalisme révolutionnaire.
Cela signifie qu'elle perpétue la forme de syndicalisme fondatrice de la
CGT en France. Avant d'être déchirée puis asservie par des partis politiques
tenant du communisme autoritaire ou de la social-démocratie, la CGT
s'est développée sur des principes que, nous, nous n'avons pas reniés
(autonomie des syndicats, fédéralisme, indépendance du mouvement social
vis à vis des partis, participation de tou-te-s  les syndiqué-e-s  à la
conduite  de l'organisation,  gestion directe des luttes par les
travailleurs-ses eux/elles-mêmes…).

Sur certaines analyses de la société: existence de classes antagonistes,
réalité de la lutte des classes, rôle néfaste du capitalisme, du libéralisme

(**la loi de la jungle**) et sur certains terrains de luttes (contre le
fascisme, l'extrême-droite, le nationalisme et le militarisme, pour les
droits des femmes, les droits des LGBT, le droit au logement pour tou-te-s, etc…),
la CNT peut côtoyer les organisations politiques dites de gauche ou
d'extrême gauche.
Pour autant, la CNT garde une stricte indépendance et autonomie vis à vis
de tous ces mouvements politiques.

Si chacun-e à la CNT est libre d'adhérer (ou non) à une organisation politique,
personne ne peut se prévaloir de ce type d'adhésion pour ce qui concerne
son activité syndicale.

Ces organisations n'ont aucun droit, pouvoir ou influence à l'intérieur
de l'organisation syndicale.
Le corollaire est que dans toute action menée par une organisation politique
à laquelle il appartiendrait, le/la syndiqué-e CNT ne peut se prévaloir
de son adhésion ou d'un mandat syndical qu'il aurait dans notre
confédération.

Exemple : un-e cénétiste qui, à titre individuel, aurait décidé de se
présenter à un scrutin politique ne peut se revendiquer de son appartenance
à la CNT et encore moins d'un mandat syndical de secrétaire de tel ou
tel syndicat.
De plus, conformément aux statuts, il/elle devra abandonner ses mandats
syndicaux (mais sans avoir à résilier son adhésion individuelle).


La CNT, un syndicat anarchiste ? La CNT, un syndicat d'anarchistes ?
======================================================================

Ce qui est vrai, c'est qu'au début du XXe siècle, deux courants d'égale
importance existaient dans le mouvement social.

L'un autoritaire (qui donnera corps à un parti comme le PCF), l'autre
anti-autoritaire qui perdra malheureusement une grande partie de son
influence après la première guerre mondiale avec l'accession au pouvoir
des communistes autoritaires en Russie (accession au pouvoir qui deviendra
pendant plusieurs décennies le modèle imposé au prolétariat international).

Pourtant, la tendance anti-autoritaire, même affaiblie, a continué d'exister
et a même donné à la mémoire sociale internationale des témoignages vivants
qu'il était possible de révolutionner une société sans aboutir au goulag.

Par opposition à ce système aberrant d'exploitation et d'asservissement
qui a caractérisé le communisme autoritaire, la CNT fait référence dans
sa charte de 1946 au **communisme libre** et, dans des textes d'orientation
plus récents, au **communisme libertaire**.

Au début du XXe siècle, des anarchistes fermement opposé-e-s à l'autoritarisme
et à l'étatisme ont compris que le syndicalisme était la forme d'organisation
qui permettait le mieux d'offrir à chacun la faculté de prendre en main
son destin.
Ils/elles ont donc adhéré en nombre à la CGT et contribué à son développement,
avant que cette confédération ne soit reprise en main par les communistes
autoritaires.

À cette même époque, d'autres travailleurs-ses qui ne portaient pas cette
référence au mouvement anarchiste (à savoir des syndicalistes révolutionnaires
et des communistes non autoritaires) ont aussi apporté leur pierre  à la
construction de notre  confédération.
Plus généralement, tous ceux et toutes celles qui, sans présupposé,  ont
trouvé  au sein de la CNT un projet social proche de leurs intérêts de
classe, de leurs préoccupations, un mode d'organisation qu'ils/elles ont
testé et pratiqué à l'épreuve de leurs luttes et des nécessités, un outil
quotidien et nécessaire pour résister, tout de suite, pour changer leur vie,
pas simplement dans l'idéal, dans le futur, mais dès demain matin ou au
plus tard cet après midi, parce qu'il y a urgence, ont enrichi notre expérience,
affiné nos analyses et élargi nos horizons.

Comment, quand on est un soi-disant spécialiste de l'histoire du mouvement
syndical, social et politique en France, peut-on ignorer à ce point la
réalité, à savoir que les organisations politiques anarchistes sont
très divisées sur l'intérêt pour leurs adhérent-e-s de se syndiquer ou non,
et que parmi les anarchistes qui choisissent de se syndiquer, certains-aines
le font à la CGT, d'autres à F0, d'autres encore à la FSU, à SUD ou à la CNT.

Depuis de nombreuses années, les salarié-e-s qui adhérent à la CNT le font
sur la base de l'originalité de son projet syndical et de sa pratique
autogestionnaire, et non pas sur la proximité avec une quelconque
organisation politique (y compris anarchiste).

On voit donc que **CNT, syndicat anarchiste**, c'est assez fantaisiste
et un peu court comme description de la réalité de notre organisation.


L'autonomie ouvrière, c'est quoi ?
====================================

Si l'expression d'autonomie ouvrière a connu son heure de gloire dans
les années 1970, la réalité qu'elle recouvre est plus ancienne.

Les syndicats de la CNT s'y réfèrent dans le sens où l'autonomie ouvrière
signifie l'organisation libre, indépendante et autonome des exploité-e-s.

Tu auras reconnu bien sûr la devise de la première internationale :
**L'émancipation des travailleurs sera l'œuvre des travailleurs eux-mêmes**.

Pour la CNT, si la classe des exploité-e-s ne doit rien à personne, elle
ne doit pas non plus attendre quoi que ce soit de l'État, de ses institutions
ou rouages, y compris des élections, du patronat, des organisations
politiques ou des intellectuels qui parlent soi-disant au nom de tou-te-s.

Puisque personne ne travaille à ta place, que personne ne décide pour toi,
que personne ne parle pour toi !

Pour la CNT, l'autonomie ne se conçoit qu'ancrée dans les quartiers,
les lieux de travail.
Elle n'a de valeur que si elle est en contact continu avec la population,
s'organisant de façon temporaire ou permanente dans et avec le syndicat.

Que cette auto-organisation prenne une forme violente ou pas est annexe
comme nous le verrons ultérieurement.
L'essentiel pour nous est qu'elle soit démocratique et qu'elle redonne
aux individu-e-s la volonté de lutter collectivement, dans un esprit
de solidarité et non d'assistanat.



La CNT, un syndicat extrémiste ?
=====================================

Le projet de justice sociale, de gestion directe des travailleurs-ses,
ce communisme libertaire auquel fait référence la CNT n'est évidemment
pas compatible avec l'organisation actuelle de la société basée sur
l'exploitation du plus grand nombre par une minorité de nanti-e-s.

La révolution sociale ne peut donc se suffire d'un simple coup de peinture
sur un édifice fondé sur l'injustice permanente.
En ce sens, la CNT est un syndicat révolutionnaire qui veut mettre en
place un changement radical de société.
Elle n'est pas simplement réformiste.

Si **extrémiste** signifie surenchère démagogique, jusqu'auboutisme,
refus de toute avancée sociale, refus de toute amélioration des conditions
de travail ou d'existence, alors il est clair que la CNT n'est pas extrémiste.

Toute avancée sociale, tout nouveau droit acquis par les travailleurs-ses
sont bons à prendre, même s'ils ne changent pas complètement et immédiatement
le désordre établi.
Ceux et celles qui qualifient la CNT de **syndicat extrémiste** préfèrent
encore une fois la facilité du lieu commun à l'analyse des faits.

L'examen rigoureux des terrains de luttes dans lesquels les syndicats
et les adhérent-e-s de la CNT sont engagé-e-s le montre.
Amélioration des conditions de travail, lutte contre la précarité, le
chômage, pour l'augmentation des plus bas salaires, tout ce qui a pu être
obtenu, tout ce qui a été tenté ne saurait suffire à remettre en cause
complètement la société actuelle.
Pourtant, chaque fois qu'ils-elles l'ont pu, les cénétistes ont pris leur
part dans ces luttes.

De plus, chaque fois que des travailleurs-ses obtiennent quelque chose
par eux/elles-mêmes, grâce à leur détermination, leur solidarité et
leur unité, c'est un peu de confiance qu'ils reprennent en eux/elles-mêmes,
un pas en avant vers la prise de conscience de la nécessité de l'action
commune, de la force de la solidarité.

La CNT, un syndicat illégaliste ?
=====================================

Les lois et particulièrement les lois dites **sociales** ne sont qu'une
réglementation des rapports sociaux à un moment donné.
Une photographie d'un rapport de force actuel dans la lutte des classes.

Les lois évoluent au fur et à mesure et au gré du poid des forces
antagonistes en présence.
Quand le patronat et ses alliés sont forts, des lois antisociales sont
votées.
Quand le mouvement social a su se mobiliser pour faire valoir ses intérêts,
la loi ne vient alors qu'enregistrer un droit que ce mouvement social a,
en fait, imposé.

La lutte contre une loi et la pratique de l'illégalité peuvent donc être
une nécessité pour faire avancer les droits du plus grand nombre.

Les cénétistes ne sont d'ailleurs pas les seuls à avoir choisi, en
certaines circonstances, de lutter contre une loi ou de la transgresser.

De la désobéissance civile prônée par Gandhi en Inde à la résistance
**illégale** pratiquée pendant les années noires de l'État français,
des réquisitions de logements vides à la revendication publique d'avoir
avorté (quand c'était encore un délit), les exemples sont nombreux
d'actions illégales, mais combien légitimes, qui ont conduit à des
libérations.

Erigée en simple principe absolu, vide de sens, en dehors de tout contexte,
la lutte contre les lois pour elle-même, comme fin en soi, n'a pas d'intérêt
pour nous anarcho-syndicalistes et syndicalistes révolutionnaires.
Encore une fois, ceux et celles qui ne veulent voir dans la CNT qu'une
organisation **d'illégalistes** feraient mieux de constater que la plus
grande partie de notre activité syndicale quotidienne consiste à essayer
de faire respecter des lois, notamment les lois du travail et que l'on
verra plus certainement les syndicalistes de la CNT plongé-e-s dans le
code du travail (dont ils-elles connaissent aussi les limites) que dans
un quelconque bréviaire anarchiste fantasmatique.


La CNT, un syndicat violent ?
==================================

La CNT organise les travailleurs-ses victimes de l'extrême violence des
rapports économiques et sociaux générée par la société capitaliste.
Les premières violences et la véritable insécurité ce sont l'exploitation,
le licenciement, le chômage, la précarité, la misère, la faim, la perte
du logement.., bref le quotidien de millions d'entre nous.

Quand, dans l'Histoire, celles et ceux qui étaient asservi-e-s se sont
dressé-e-s pour se faire respecter, pour faire valoir leurs droits,
ils-elles ont eu à faire non à des interlocuteurs-trices bienveillant-e-s,
des **hommes et femmes de dialogue**, mais à des forces de répression.
C'est bien plus souvent la matraque (ou le fusil) qui sert d'argument
aux classes possédantes.

La réplique violente et collective à la répression, à l'asservissement,
est bien évidemment légitime.
C'est même un réflexe naturel, à moins de prôner le masochisme ou la
soumission comme forme de réponse à la violence étatique ou patronale.
On peut feindre de le regretter, on peut faire semblant de l'ignorer,
mais si les serfs d'autrefois s'étaient contenté-e-s de chanter des
Carmagnoles, nous viverions sans doute encore sous le règne de la féodalité.

L'utilisation de la violence par les exploité-e-s n'est donc pas un choix
moral fait par des individus en dehors de tout contexte économique ou
politique.
C'est souvent la réponse imposée par l'intransigeance des exploiteurs.

Ce recours obligé à la violence n'a rien à voir avec le terrorisme, qui
n'est que l'utilisation individuelle, aveugle, d'actions violentes au
service des intérêts ou des délires politiques de groupuscules qui
s'autoproclament révolutionnaires ou d'avant-garde.

Pour autant, l'amalgame entre actions violentes et actions terroristes
est depuis toujours une spécialité des médias quand ils sont au service
des forces réactionnaires.
À en lire la presse française des années 1940-1944, l'histoire de la
Résistance n'est que la litanie des actions violentes menées par ceux
et celles que le pouvoir qualifiait alors de **terroristes**.

La CNT, on l'aura compris, n'est ni **violente** ni **non-violente**.
Elle tente d'organiser les travailleurs-ses pour leur émancipation
et n'a donc pas pour projet de les désarmer face à des adversaires
qui n'ont, eux, aucun scrupule pour les réprimer.

Elle n'est évidemment pas non plus le réceptacle naturel de tous celles
et ceux pour qui l'usage des armes ou de la violence tient lieu de
réflexion et de pratique émancipatrice.



La CNT, une organisation spontanéiste ? basiste ? assembléiste ?
=====================================================================

Tous ces qualificatifs recouvrent en partie le mode de fonctionnement
de la CNT. Aucun n'est suffisant.

Spontanéiste ? Oui, s'il s'agit de la liberté de décider sans subir
la pression d'une organisation politique ou d'un quelconque groupe
constitué extérieur à la CNT. Certainement pas si cela doit signifier
que nous décidons individuellement, sans réflexion collective préalable,
sans recherche d'une identité de vue ou d'un accord sur les décisions
à prendre.

Primauté de l'individu ou du collectif ? À l'intérieur du syndicat, la
voix et l'opinion de n'importe quel-le adhérent-e vaut la voix de n'importe
quel-le autre. Le principe est simple : pas de membres d'honneur, pas
de voix prépondérante, un individu = une voix.
Par contre, dès lors qu'il s'agit de l'activité confédérale et à tous
les niveaux d'organisation de cette confédération (union régionale de
syndicats, union locale de syndicats, fédération de syndicats de branche,
confédération de syndicats), le principe est collectif (un syndicat = une voix).

La CNT est une confédération de syndicats et non une simple association
d'individus. Les débats qui la traversent sont toujours issus de la
réflexion collective des syndicats.

Le congrès confédéral de la CNT, qui se tient tous les deux ans, n'est
pas une sorte de grande assemblée générale d'individus où chacun pourrait
parler et décider en son nom propre.

Il en est de même pour les congrès régionaux de syndicats.
L'un et l'autre réunissent des délégué-e-s mandaté-e-s par leur syndicat
qui débattent et décident au nom de leur syndicat et sous le contrôle de
celui-ci. Ces délégué-e-s sont révocables à tout moment.
Il n'y a pas de représentant-e permanent-e d'un syndicat.
La rotation des tâches et des responsabilités est un principe de
fonctionnement de notre organisation syndicale.

Personne ne peut passer par dessus l'avis donné par son syndicat pour
faire prévaloir un avis personnel qui n'aurait pas été retenu par son
organisation de base, le syndicat.
Outre qu'il habitue chacun-e à débattre de ses choix avec les autres
membres de son syndicat (ce qui est le moins que l'on puisse demander
dans une organisation censée aboutir à des prises de décision collective),
ralliant le plus grand nombre d'individus, ce mode de fonctionnement
empêche que l'organisation syndicale ne devienne un terrain propice
pour les ambitions ou les volontés de pouvoir de quelques individus.

On ne fait pas de carrière à la CNT.
Il n'y a pas de pouvoir à prendre.

Il est consternant (ou comique) de voir que d'autres confédérations
tolèrent ou s'habituent au fait que tel ou tel de leur responsable,
généralement leur secrétaire confédéral, soit appelé par les médias
**le patron** de leur organisation.

Basiste, assembléiste ?
==========================

Oui, quand cela fait référence au choix des assemblées générales
décisionnelles comme mode d'organisation dans les luttes, dans les
grèves.
Oui, quand il s'agit de faire en sorte qu'aucune direction politique,
qu'aucun mot d'ordre parachuté de l'extérieur ne viennent parasiter
les décisions qu'ont à prendre les travailleurs-ses eux/elles-mêmes.

Non évidemment, quand il s'agit de court-circuiter l'organisation
collective de base de la CNT qu'est le syndicat.


Le fédéralisme, c'est quoi ?
==================================

Confédération de syndicats, la CNT considère que le fédéralisme est une
composante essentielle de son mode de fonctionnement.

**Le fédéralisme s'oppose au centralisme**.

Il signifie que ce sont toujours les syndicats, fédérés entre eux, qui
décident, et non un bureau central dépositaire de la vérité révélée.

Les prises de décision se font en assemblée générale de syndicat.
Les orientations de la confédération sont décidées en congrès de syndicats.

Le bureau confédéral qui y est élu est simplement mandaté pour veiller à
l'application des orientations.

Ces décisions adoptées par tou-te-s ont été aussi proposées par les
syndicats qui, quelques mois avant le congrès, ont déposé des motions.

Chez nous pas de texte d'orientation rédigé pour la **base** par des
**dirigeant-e-s** ou par des courants et fractions.

Ce fonctionnement fédéral, sans base ni sommet, évite la mise en place
d'instances de pouvoir, les différents bureaux élus (bureau confédéral,
bureaux régionaux et fédéraux) ont pour mission de faire circuler
l'information entre les syndicats, de coordonner les actions et de
permettre l'application des décisions des syndicats.

Cela ne veut pas dire que chaque syndicat peut faire tout et n'importe
quoi sans se soucier des autres.

Tout d'abord, chaque syndicat rencontre les autres syndicats dans les
AG d'unions locales, d'unions régionales, dans les congrès fédéraux ou
confédéraux.

Une fois qu'une décision y a été adoptée, elle est reconnue et appliquée
par tous les syndicats concernés, sauf si il a été précisé que sur tel
ou tel point le syndicat pouvait faire valoir son autonomie (par exemple
l'appel d'un syndicat à une manifestation peut ne pas être suivi par une
autre structure de la CNT). Au-delà de ces décisions de congrès, tous
les syndicats sont liés entre eux par un pacte associatif qui définit
l'autonomie de chacun et l'indispensable solidarité.
La cohérence du fédéralisme est inscrite dans les statuts de la CNT que
chaque syndicat s'engage à respecter en se confédérant.
Ce sont ces statuts qui en dernier recours fixent la vie collective des
syndicats.


La CNT, un groupement d'utopistes ?
=======================================

Une utopie sociale pourrait être définie comme une théorie, un ensemble
de constructions intellectuelles visant à organiser la société et qui n'a
jamais été mis en pratique nulle part dans le passé ou dont la mise en
application pour le présent ou le futur est jugée impossible, irréaliste,
irréalisable.

D'un autre point de vue, ce peut être une vision même lointaine vers
laquelle tend le combat humain et qui sert à la fois d'aiguillon, et
**d'étoile du berger** qui, même inaccessible, est utile et utilisée
pour garder un cap.

**Jamais, nulle part ?**
---------------------------

Occultées, ignorées par l'Histoire officielle, toujours refoulées par
ceux et celles qui ont participé à leur anéantissement (l'alliance des
réactionnaires nantis, des conservateurs, des socio-démocrates et des
communistes autoritaires), des mises en pratique du communisme libertaire
ont jalonné l'histoire contemporaine (de la Commune de Paris à la révolte
de Cronstadt, des combats des partisans de Makhno aux soulèvements
de Hongrie en 1956).

L'exemple le plus consistant de ces utopies vivantes reste, pour nous,
la révolution sociale espagnole de 1936 à 1939.

Pendant cette période, des millions de prolétaires espagnol-e-s, levé-e-s
contre le coup d'État fasciste de Franco, ont su trouver les énergies,
l'imagination, les compétences et la générosité pour se battre, même
abandonné-e-s du reste de l'Europe, contre le fascisme et le nazisme.

Malgré les pires conditions d'une situation de guerre, ils-elles ont
su mettre en œuvre et organiser une société de type communiste libertaire,
démontrant que ce projet d'organisation économique et politique pourtant
complexe, pourtant exigeant en terme de participation de tous-tes au
bien commun, était réalisable.

Nous sommes là loin de théories fumeuses, d'idéalismes généreux mais
sans aucun lien avec la réalité quotidienne, de projets inapplicables,
bref d'une utopie telle qu'on la définit habituellement dans son sens
le plus réducteur.
Oui, là, des milliers d'expériences ont vu le jour, collectivisant les
terres dans un pays appartenant aux seuls grands propriétaires agraires,
socialisant les entreprises et les fabriques, associant chaque producteur-trice
qui le souhaitait à la gestion de son travail et au partage collectif
des richesses qui en sont le fruit, autogérant (avant la lettre) les
communautés humaines, villes et villages et trouvant encore les

ressources pour se battre sur tous les fronts contre les armées fascistes
(espagnoles, allemandes ou italiennes).

Nous, anarcho-syndicalistes et syndicalistes révolutionnaires de la CNT,
sommes armé-e-s de ces expériences, de cette mémoire, pour répondre à
toutes celles et ceux qui ont prétendu, ou prétendent encore, que la
révolution sociale est une chimère dont personne n'aurait jamais vu
l'ombre du commencement, d'une mise en pratique.

Mais nous ne sommes pas pour autant les héritier-e-s de cette vivante
utopie sociale. D'abord parce qu'un projet social pour un autre futur
ne se met pas en place avec un patrimoine et des expériences historiques,
même riches de leurs avancées et de leurs erreurs.

Sans passéisme ni nostalgie révolutionnaire romantique, conscient-e-s que
l'histoire ne repasse jamais les mêmes plats, que les sociétés ont
changé et que notre effort ne sera couronné de succès qu'à partir de nos
luttes actuelles, de nos capacités à nous organiser aujourd'hui, nous
restons indécrottablement des **partageux -euses**, certain-e-s qu'il n'y a
pas de plus actuelle, de plus raisonnable, de plus exaltante modernité.

Qu'on tente de nous démontrer que cette aspiration est irréalisable
et nous nous attachons quotidiennement à prouver le contraire.

Les plus acharné-e-s de nos détracteurs-trices savent bien que la
révolution sociale est du domaine du possible. On comprend que pour
protéger leurs coffres-forts, leurs actions, leurs pouvoirs et leurs
privilèges, ils-elles n'aient de cesse de faire croire qu'il s'agit
d'une chimère.

De peur que grossisse le nombre de ceux et celles qui réalisent
qu'on peut et qu'on doit se passer de leur caste.


La CNT, une organisation corporatiste ?
============================================

Le fait que toute l'organisation de la CNT repose sur le syndicat
professionnel pourrait peut-être le faire croire.
Les syndiqué-e-s le sont en fonction de leur activité économique, de
leur travail.

Quand on travaille à La Poste, on se syndique au syndicat PTT de son
département.

Sommes-nous pour autant groupé-e-s par **corporation** ?
----------------------------------------------------------

Le type de syndicat choisi par la CNT est le syndicat d'industrie et non
le syndicat de métier.

Nous fonctionnons selon le principe **un lieu = un syndicat**.

L'ensemble des travailleurs-ses d'un même site (une entreprise, un établissement...)
se retrouvent dans la même section et, au niveau du département, ils et
elles sont dans le même syndicat.
Ainsi, par exemple, un syndicat éducation regroupe des personnels de tous
les degrés (de la maternelle à l'université), de tous les statuts
(précaires ou fonctionnaires) et de tous les métiers (enseignant-e-s,
psychologues scolaires, documentalistes, etc.).

Bien sûr, qu'on travaille dans l'enseignement public ou dans l'enseignement
privé, c'est dans la même structure qu'on se retrouve.
Cela évite ce corporatisme qui consiste à opposer un corps ou un statut
à un autre. Nous appartenons tou-te-s à la même classe, celle des
travailleurs-ses.

L'organisation de la CNT est donc non-corporatiste mais, de plus, elle
combat ce corporatisme en mettant en avant la suppression des inégalités
salariales et des hiérarchies.

Nous ne défendons pas un statut contre un autre.

C'est le sens de notre engagement incessant aux côtés des collègues
précaires, non-titulaires...

La CNT, en plus des syndicats d'industrie (santé-social, métallurgie,
collectivités territoriale...), a créé des syndicats dits **interco**,
ou **interpro** ou **des services et de l'industrie** qui organisent
des salarié-e-s qui ne sont pas encore assez nombreux-ses au sein de la
CNT pour se déclarer en syndicat d'industrie. Dans ces intercos,
les adhérent-e-s militent, comme tou-te-s les membres de la CNT, dans
leur entreprise et bénéficient du soutien des autres adhérent-e-s en
attendant de créer leur propre structure syndicale.

Mais le rôle des intercos n'est pas d'être l'alibi intercorporatiste
pour les autres syndiqué-e-s de la CNT.

La CNT repose sur deux pieds : l'organisation des salarié-e-s sur leur
lieu de travail (le syndicat) et leur organisation dans leur quartier (l'union
locale).

Militer à la CNT, c'est sortir aussi de son entreprise pour agir dans
son quartier et intervenir aux côtés des autres travailleurs-ses sur
leur lieu de travail.

La CNT est une confédération de syndicats qui touchent tous les secteurs
(privé, public, primaire, secondaire, tertiaire).

Dans les unions locales, dans les unions régionales... chacun-e rencontre
des salarié-e-s d'autres secteurs et lutte avec eux/elles, leur file un
coup de main.

C'est pratique – et efficace – par exemple pour distribuer des tracts
lorsque telle ou telle section ne peut pas encore se montrer au grand
jour face au patron.


Confédération ? Nationale ? du Travail ?
============================================

Quelques explications...

**Confédération** ?
----------------------

Ce terme signifie que notre organisation confédère,
c'est à dire regroupe, des syndicats de différents secteurs, de différentes
régions.

A l'origine, la CGT d'avant 1914 (l'organisation syndicale dont nous
nous réclamons sur le plan historique) s'est constituée en confédération
par la fusion des Bourses du Travail (organisation des producteurs-trices
en fonction de leur lieu d'habitation) et des syndicats (organisation
économique sur le lieu de travail).

**Nationale** ?
----------------

À sa création en 1946, les militant-e-s ont choisi de nommer leur
organisation **Confédération Nationale du Travail** en hommage bien sûr
à la CNT espagnole et l'action qu'elle a menée entre 1936 et 1939.

La présence de certain-e-s réfugié-e-s espagnol-e-s en exil n'y est
pas non plus pour rien.
Plutôt que de reprendre le sigle CGT-SR (Syndicaliste Révolutionnaire),
scission de la CGT-U créée en 1926 mais interdite en 1939 par le régime
de Vichy et les nazis, ils et elles ont opté pour celui de CNT.

L'adjectif **nationale** recouvrait en fait une réalité spécifique à
l'Espagne où le découpage en régions était très fort.

En créant la CNT espagnole, les ouvriers-ières de ce pays entendaient
signifier leur refus des régionalismes, synonymes à l'époque de réaction,
les ouvriers-ières se sentant membres d'une communauté plus vaste.

Nous sommes donc loin de l'exaltation d'un sentiment national.

Pour nous, nul relent de nationalisme ou de patriotisme.

Ce n'est pas à la CNT que l'on chantera la Marseillaise.

**Nationale** décrit simplement notre champ d'action géographique.

Par ailleurs, la CNT se réclame de l'esprit de l'Association Internationale
des Travailleurs (AIT), organisation créée en 1864 et plus connue sous le
nom de  **Première internationale**.
La CNT œuvre activement au niveau international (actions de solidarité,
échange d'infos, luttes et manifestations internationales…) pour démontrer
que les travailleurs-ses n'ont ni patrie ni pays.


**Travail** ?
----------------

Autre source d'étonnement pour certaines personnes ne nous connaissant
pas ou peu.
Le mot **travail** ne signifie pas que nous ne syndiquons pas les chômeur-ses,
les retraité-e-s, les lycéen-ne-s, les étudiant-e-s.

Au contraire, ce sont pour nous des travailleurs-euses soit en formation,
soit au repos, soit à la recherche de travail.

Le mot **travail** ne correspond pas non plus à une mise en avant du
travail comme valeur morale (au sens où d'autres, aux antipodes de nos
conceptions, l'ont fait avec leur **Travail, Famille, Patrie**).

En fait le terme **Travail** a été choisi à la place du mot **Travailleurs**
parce qu'il marque notre appartenance à un camp : celui du Travail,
par opposition à l'autre camp, celui du Capital.

Le chat, le drapeau rouge et noir
===================================

Profitons de cette page pour dire quelques mots de l'image du chat, souvent
associée au sigle CNT…

Avec le drapeau rouge et noir (inventé par la CNT espagnole à l'occasion
du 1er mai 1931 à Barcelone), le dessin représentant un chat noir – hérissé
et toutes griffes dehors – est l'un des deux principaux symboles de
l'anarcho-syndicalisme et du syndicalisme révolutionnaire.

Les origines de ce symbole ne sont pas claires mais, selon une anecdote,
il proviendrait d'une grève menée aux USA au début du XXe siècle par les
IWW (Industrial Workers of the World).

Alors que plusieurs militant-e-s étaient tabassé-e-s et conduit-e-s à
l'hôpital, un chat noir maigrelet s'installa dans le campement des
grévistes.
Ces derniers-ières nourrirent le chat qui reprit force à mesure que la
grève tournait en faveur des travailleurs-euses.
Finalement, les travailleurs-euses virent quelques-unes de leurs demandes
satisfaites et adoptèrent le chat comme mascotte.

Le chat noir a été dessiné pour la première fois par Ralph Chaplin,
militant des IWW.
Comme sa position le suggère, il symbolise le combat et par extension
le syndicalisme de lutte.

De plus, dans l'imaginaire collectif, le chat est souvent associé aux
idées de liberté et d'indépendance.

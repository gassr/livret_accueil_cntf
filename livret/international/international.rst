

.. index::
  pair: Livret d'accueil; International

.. _livret_accueil_international:

========================================================
Le secteur international
========================================================


La CNT mène un combat sur une base internationaliste, met en avant la solidarité
avec celles et ceux qui luttent de par le monde, qui sont répriméEs, qui ont
besoin d’actions solidaires pour épauler leur lutte revendicative, parfois
même pour le seul droit de s’associer en syndicats.

Aussi chaque structure peut-être amenée à développer une activité à ce niveau.
La fédération des activités du courrier et de la téléphonie par exemple
s’efforce de tisser des liens avec des syndicats de lutte qui agissent dans la
même branche professionnelle et essaie de participer à des rencontres
internationales de branches pour favoriser la coordination de la lutte et de
la solidarité.

Au sein de la Confédération, il existe un Secrétariat international qui
fonctionne avec des groupes spécifiques organisés par continent ou secteur:

- groupes Amérique latine, Europe, Afrique, Asie, Palestine, etc.

Ce secteur est essentiel pour l’engagement de la CNT sur le plan international.

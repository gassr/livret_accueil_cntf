
.. index::
   pair: Livret d'accueil; Cotiser
   ! Cotisation

.. _adherer_cotiser:

==============================================================
Adhérer c'est aussi cotiser mais à quoi sert la cotisation ?
==============================================================

.. contents::
  :depth: 3

Préambule
===========

**Payer sa cotisation est une nécessité**, c’est l’acte indispensable qui permet
de revendiquer son appartenance à la CNT, de recevoir des informations du
syndicat, de participer aux réunions, aux débats et aux prises de décisions.

**Est donc considérée comme adhérente toute personne à jour de cotisations**.

Mais c’est également une contribution financière qui permet à la CNT de
fonctionner.
La CNT contrairement à toutes les autres organisations syndicales
est la seule dont les ressources proviennent uniquement de ses adhérents:
**aucune subvention des collectivités territoriales, de l’État ou des
entreprises privées**.

**La cotisation permet au syndicat de fonctionner**
=======================================================

Le montant de la cotisation est fixé par chaque syndicat, normalement 1%
du salaire net, avec des aménagements pour les précaires, pour les chômeurs,
les personnes en formation professionnelle, les étudiants, ou dans des
situations ponctuelles particulières à voir avec l’assemblée générale
du syndicat.

L’adhérent verse sa cotisation au trésorier du syndicat. Cette cotisation
peut s’effectuer mensuellement, trimestriellement ou annuellement, par chèque,
espèces ou prélèvement. En échange il reçoit sa carte syndicale, ses timbres
syndicaux qui attestent du paiement de la cotisation.

L’argent de la cotisation permet au syndicat d’assurer son fonctionnement:

- tirage de tracts, d’affiches
- paiement d’assurances pour les locaux ;
- achat de livres
- achat de matériel informatique,
- abonnement internet.

Le syndicat a la possibilité également de se constituer une caisse de grève
ou de solidarité pour ses membres.

Mais une partie de la cotisation ne reste pas au syndicat, elle est ventilée
vers les différentes structures qui composent la CNT.


.. figure:: images/bandoconf.jpg
   :align: center

.. _livret_accueil_syndicat:

===============================================
Livret d'accueil
===============================================

.. toctree::
   :maxdepth: 2

   livret/livret
   2017/2017

.. toctree::
   :maxdepth: 1
   :caption: Meta

   meta/meta
